//
//  ViewController.swift
//  TipCalculator2
//
//  Created by admin1 on 3/17/15.
//  Copyright (c) 2015 faculty. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var billAmountTF: UITextField!
    @IBOutlet weak var tipPercentTF: UITextField!
    @IBOutlet weak var tipAmountTF: UITextField!
    @IBOutlet weak var totalTF: UITextField!
    
    var currencyFormatter: NSNumberFormatter = NSNumberFormatter()
    var percentFormatter: NSNumberFormatter = NSNumberFormatter()
    var decimalFormatter: NSNumberFormatter = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        currencyFormatter.generatesDecimalNumbers = true
        currencyFormatter.alwaysShowsDecimalSeparator = true
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.maximumFractionDigits = 2
        
        percentFormatter.numberStyle = NSNumberFormatterStyle.PercentStyle
        percentFormatter.generatesDecimalNumbers = true
        
        decimalFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        decimalFormatter.generatesDecimalNumbers = true
        decimalFormatter.maximumFractionDigits = 0

        billAmountTF.text = stringFromNumber(NSDecimalNumber(string: "0.00"), currencyFormatter)
        tipPercentTF.text = stringFromNumber (NSDecimalNumber(string: "15"), decimalFormatter)
        tipAmountTF.text = stringFromNumber(NSDecimalNumber(string: "0.00"), currencyFormatter)
        totalTF.text = stringFromNumber(NSDecimalNumber(string: "0.00"), currencyFormatter)

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func billAmountChanged(sender: AnyObject) {
        println("billAmountChanged bill:\(billAmountTF.text), tip%: \(tipPercentTF.text)")
        billAmountTF.text = fixCurrencyString (billAmountTF.text)
        println("new bill:\(billAmountTF.text)")
        tipAmountTF.text = calculateTipAmountFrom(billAmountText: billAmountTF.text, tipPercentText: tipPercentTF.text)
        totalTF.text = calculateTotalFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
    }

    @IBAction func tipPercentChanged(sender: AnyObject) {
        println("tipPercentChanged bill:\(billAmountTF.text), tip%: \(tipPercentTF.text)")
        tipAmountTF.text = calculateTipAmountFrom(billAmountText: billAmountTF.text, tipPercentText: tipPercentTF.text)
        totalTF.text = calculateTotalFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
    }
    
    @IBAction func tipAmountChanged(sender: AnyObject) {
        println("tipAmountChanged bill:\(billAmountTF.text), tip:\(tipAmountTF.text)")
        tipAmountTF.text = fixCurrencyString (tipAmountTF.text)
        println("new tip:\(tipAmountTF.text)")
        tipPercentTF.text = calculateTipPercentFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
        totalTF.text = calculateTotalFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
    }

    @IBAction func totalChanged(sender: AnyObject) {
        println("totalChanged bill:\(billAmountTF.text), total:\(totalTF.text)")
        totalTF.text = fixCurrencyString (totalTF.text)
        println("new total:\(totalTF.text)")
        tipAmountTF.text = calculateTipAmountFrom(billAmountText: billAmountTF.text, totalText: totalTF.text)
        tipPercentTF.text = calculateTipPercentFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
    }
    
    
//   func updateTip(sender: AnyObject) {
//        println("updateTip bill:\(billAmountTF.text), tip%: \(tipPercentTF.text)")
//        tipAmountTF.text = calculateTipAmountFrom(billAmountText: billAmountTF.text, totalText: totalTF.text)
//        tipPercentTF.text = calculateTipPercentFrom(billAmountText: billAmountTF.text, tipAmountText: tipAmountTF.text)
////        billAmountTF.text = stringFromNumber (numberFromString (billAmountTF.text, currencyFormatter), currencyFormatter)
//    }
    
    func calculateTipPercentFrom (#billAmountText: String, tipAmountText: String) -> String {
        println("updateTipAmount bill:\(billAmountTF.text), tip%: \(tipPercentTF.text)")
        let billAmount = numberFromString(billAmountText, currencyFormatter)
        let tipAmount = numberFromString(tipAmountText, currencyFormatter)
        let tipPercent = tipAmount / billAmount
        println("bill: \(billAmount) & tip: \(tipAmount) => tip%: \(tipPercent)")
        return stringFromNumber (tipPercent * 100, decimalFormatter)
    }
    
    func calculateTotalFrom (#billAmountText: String, tipAmountText: String) -> String {
        let billAmount = numberFromString(billAmountText, currencyFormatter)
        let tipAmount = numberFromString(tipAmountText, currencyFormatter)
        let total = billAmount + tipAmount
        println("bill: \(billAmount) & tip: \(tipAmount) => total: \(total)")
        return stringFromNumber (total, currencyFormatter)
    }
    
    func calculateTipAmountFrom (#billAmountText: String, totalText: String) -> String {
        let billAmount = numberFromString(billAmountText, currencyFormatter)
        let total = numberFromString(totalText, currencyFormatter)
        let tipAmount =  total - billAmount
        println("bill: \(billAmount) & total: \(total) => tip: \(tipAmount)")
        return stringFromNumber (tipAmount, currencyFormatter)
    }
    
    func calculateTipAmountFrom (#billAmountText: String, tipPercentText: String) -> String {
        let billAmount = numberFromString(billAmountText, currencyFormatter)
        let tipPercent = numberFromString(tipPercentText, decimalFormatter)
        let tipAmount =  billAmount * tipPercent / 100
        println("bill: \(billAmount) & tip%: \(tipPercent) => tip: \(tipAmount)")
        return stringFromNumber (tipAmount, currencyFormatter)
    }
    
    func fixCurrencyString(string: String) -> String {
        var n = numberFromString (string, currencyFormatter) ?? 0
        
        let index = advance(string.endIndex, -2)
        let range = Range(start: index, end: index.successor())
        let substring = string.substringWithRange(range)
        println("substring \(substring)")
        if "." == substring {
            n = (n / 10)!
        }
        else {
            n = (n * 10)!
        }
        println("fixCurrencyString \(n)")
        
        let currencyText = stringFromNumber(n, currencyFormatter)
        return currencyText
    }
}

func / (numerator: NSDecimalNumber?, denominator: NSDecimalNumber?) -> NSDecimalNumber? {
    if numerator != nil && denominator != nil && denominator != 0 {
        return numerator!.decimalNumberByDividingBy(denominator!)
    }
    return nil
}

func * (n1: NSDecimalNumber?, n2: NSDecimalNumber?) -> NSDecimalNumber? {
    if n1 != nil && n2 != nil {
        return n1!.decimalNumberByMultiplyingBy(n2!)
    }
    return nil
}

func + (n1: NSDecimalNumber?, n2: NSDecimalNumber?) -> NSDecimalNumber? {
    if n1 != nil && n2 != nil {
        return n1!.decimalNumberByAdding(n2!)
    }
    return nil
}

func - (n1: NSDecimalNumber?, n2: NSDecimalNumber?) -> NSDecimalNumber? {
    if n1 != nil && n2 != nil {
        return n1!.decimalNumberBySubtracting(n2!)
    }
    return nil
}

func stringFromNumber (number: NSDecimalNumber?, formatter: NSNumberFormatter) -> String {
    println("stringFromNumber number: \(number)")
    if number != nil {
        let str = formatter.stringFromNumber(number!)
        println("stringFromNumber str: \(str)")
        return str!
    }
    return ""
}

func numberFromString (str: String, formatter: NSNumberFormatter) -> NSDecimalNumber? {
    println("numberFromString str: \(str)")
    var n:NSNumber? = formatter.numberFromString(str) ?? NSDecimalNumber(string: str)
    if n == NSDecimalNumber.notANumber() {
        n = nil
    }
    println("numberFromString n: \(n)")
    return n as? NSDecimalNumber
}

