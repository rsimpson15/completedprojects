# Game Master's Assistant
Vision Statement
> The Game Master’s Assistant is an application to help Game Masters (also known as Storytellers or Dungeon Masters) to focus on players and their story instead of mathematics and computation. It abstracts combat actions for initiative rounds, monster attacks, monster health tracking, NPC (non-player character) attacks and NPC health tracking so that the Game Master need only focus on results and the player’s activities in the combat rounds. Unlike other applications, such as Hero Lab or Roll 20 the goal is not to take the entire game play experience and translate it into an application, and unlike applications like Combat Manager it will include options for NPCs. It will not be focused on translating game play into an online experience but rather removing time consuming mathematics and rolls from the Game Master’s concerns, so they can focus on telling their story and engaging their players at the table. This application will be delivered on iOS platforms.

Developer: [Rochelle Simpson](http://rochellesimpson.com/)

### Software Construction Process
  - Agile Software Construction
  - Scrum Process

### Tools
  - Xcode
  - BitBucket
  - Microsoft SQL Server Management Studio
  - Visual Studio Team Services

### Deployment
  - BlueHost

### Release Schedule
| Iteration | Version | Dates |
| ------ | ------ | ------ |
| Sprint 1 | Version 0.1 | Feb 26 - Mar 11 |
| Sprint 2 | Version 1.0 | Mar 12 - Mar 18 & April 2 - April 8 |
| Sprint 3| Version 2.0 | April 9 - April 22 |
| Sprint 4 | Version 3.0 | April 23 - May 6 |
| Alpha Testing | Version 3.5 | May 7 - May 9 |
| Sprint 5 | Version 4.0 | May 7 - May 20 |
| Beta Testing | Version 4.5 | May 24 - May 30 |
| Sprint 6 | Version 5.0 - Final | May 21 - June 3 |