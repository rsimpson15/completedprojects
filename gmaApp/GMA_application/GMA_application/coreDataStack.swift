//
//  coreDataStack.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 4/29/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import Foundation
import CoreData

class coreDataStack {
    var container: NSPersistentContainer{
        let container = NSPersistentContainer(name: "Monsters")
        container.loadPersistentStores{ (description, error) in
            guard error == nil else {
                print("Error: \(error!)")
                return
            }
        }
        return container
    }
    
    var managedContext: NSManagedObjectContext{
        return container.viewContext
    }
}
