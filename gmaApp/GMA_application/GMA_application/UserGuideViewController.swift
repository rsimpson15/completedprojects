//
//  UserGuideViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/4/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class UserGuideViewController: UIViewController {
    
    static func storyboardInstance() -> UserGuideViewController? {
        let storyboard = UIStoryboard(name: "UserGuideViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? UserGuideViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
 
    @IBAction func back_btn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
   
    
}

