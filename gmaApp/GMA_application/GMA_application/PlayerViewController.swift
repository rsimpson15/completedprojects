//
//  PlayerViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 5/7/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit
import CoreData

class PlayerViewController: UITableViewController {

    var resultsController: NSFetchedResultsController<Player>!
    let CDSPlayer = coreDataStackPlayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = true

        let request: NSFetchRequest<Player> = Player.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "level", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        resultsController = NSFetchedResultsController(
            fetchRequest: request,
            managedObjectContext: CDSPlayer.managedContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        resultsController.delegate = self
        
        do{
            try resultsController.performFetch()
        } catch {
            print("Perform Fetch Error: \(error)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return resultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ _tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath)
        let selectedIndexPaths = tableView.indexPathsForSelectedRows
        let rowIsSelected = selectedIndexPaths != nil && selectedIndexPaths!.contains(indexPath)
        cell.accessoryType = rowIsSelected ? .checkmark : .none
        let player = resultsController.object(at: indexPath)
        cell.textLabel?.text = player.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "player2form", sender: tableView.cellForRow(at: indexPath))
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style:.destructive, title: "Delete"){(action, view, completion) in
            let player = self.resultsController.object(at: indexPath)
            self.resultsController.managedObjectContext.delete(player)
            do {
                try self.resultsController.managedObjectContext.save()
                self.errorMessage(message: "Player Character Deleted.");
                completion(true)
            } catch {
                print("Delete Failed: \(error)")
                self.errorMessage(message: "Failed to Delete Player Character.");
                completion(false)
            }
        }
        action.image = UIImage(named: "trash")
        action.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
       
        if let _ = sender as? UIBarButtonItem, let vc = segue.destination as? AddPlayerViewController{
            vc.managedContext = resultsController.managedObjectContext
        }
        
        if let cell = sender as? UITableViewCell, let vc = segue.destination as? AddPlayerViewController{
            vc.managedContext = resultsController.managedObjectContext
            if let indexPath = tableView.indexPath(for: cell){
                let player = resultsController.object(at: indexPath)
                vc.player = player
            }
        }
    }
    
    //Error Function
    func errorMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        alert.addAction(okAction);
        self.present(alert, animated: true, completion:nil);
    }
}

extension PlayerViewController: NSFetchedResultsControllerDelegate{

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type{
        case .insert:
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath{
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath){
                let player = resultsController.object(at: indexPath)
                cell.textLabel?.text = player.name
            }
        default:
            break
        }
    }

}
