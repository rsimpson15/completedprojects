//
//  AboutViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/4/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    static func storyboardInstance() -> AboutViewController? {
        let storyboard = UIStoryboard(name: "AboutViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? AboutViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func back_btn(_ sender: UIButton) {
            dismiss(animated: true, completion: nil)
    }
 
}
