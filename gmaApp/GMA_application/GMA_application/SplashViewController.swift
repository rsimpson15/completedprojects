//
//  ViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/4/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    static func storyboardInstance() -> SplashViewController? {
        let storyboard = UIStoryboard(name: "SplashViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? SplashViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func about_btn(_ sender: UIButton) {
        performSegue(withIdentifier: "splash2about", sender: self)
    }
    
    @IBAction func userGuide_btn(_ sender: UIButton) {
        performSegue(withIdentifier: "splash2userGuide", sender: self)
    }
    
    @IBAction func logIn_btn(_ sender: UIButton) {
        performSegue(withIdentifier: "splash2logIn", sender: self)
    }
    
    
    @IBAction func reg_btn(_ sender: UIButton) {
        performSegue(withIdentifier: "splash2reg", sender: self)
    }
}

