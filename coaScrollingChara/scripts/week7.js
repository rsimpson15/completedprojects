/*File Name: week7.js  
Date: 05/15/2014 
Programmer: Rochelle Simpson*/

$(document).ready(function(){
	$('#news-carousel').jcarousel({
		vertical:true,
		scroll:1,
		wrap:'circular',
		auto: 3,
		animation: 600
	});
});
