#!/usr/bin/env python3

import pytest
from flask import Flask, jsonify, abort, request, url_for
from datetime import datetime
from mongoengine import connect, StringField, IntField, Document, DateTimeField
from pymongo import *
import sys

connect(db="activity_log_test", host="localhost")
client = MongoClient('localhost', 27017)
db = client.activity_log

def test_notnull():
    db.drop_collection("activity_log_test")

    db.activity_log.insert_one({"user_id": 12345, "username": "temp", "details": "Woot"})
    arraysize = db.activity_log.find()
    size = sys.getsizeof(arraysize)

    assert size > 0
    assert not db.activity_log.find({"user_id": 12345}) == None


def test_data_asexpected():
    db.drop_collection("activity_log_test")

    db.activity_log.insert_one({"user_id": 12345, "username": "temp", "details": "Woot"})
    user_id = db.activity_log.distinct("user_id")
    username = db.activity_log.distinct("username")
    details = db.activity_log.distinct("details")

    assert user_id == [12345]
    assert username == ["temp"]
    assert details == ["Woot"]

def test_data_notasexpected():
    db.drop_collection("activity_log_test")

    db.activity_log.insert_one({"user_id": 12345, "username": "temp", "details": "Woot"})
    user_id = db.activity_log.distinct("user_id")
    username = db.activity_log.distinct("username")
    details = db.activity_log.distinct("details")

    assert user_id != [54321]
    assert username != ["Buwah"]
    assert details != ["Shezam"]