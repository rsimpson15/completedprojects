#!/bin/bash
export PYTHONPATH=.
export ACTIVITY_SETTINGS=$(pwd)/test.settings
export FLASK_ENV=test
export FLASK_DEBUG=0
coverage run --source "." -m py.test
coverage html
open htmlcov/index.html
