from flask import Flask, jsonify, abort, request, url_for
from datetime import datetime
from mongoengine import Document, connect, IntField, StringField, DateTimeField
import os
import logging
import time

app = Flask(__name__)

#Class Struct for Activity Logger
class ActivityLogger(Document):
    user_id = IntField(required=True)
    username = StringField(required=True, max_length=64)
    timestamp = DateTimeField(default=datetime.utcnow)
    details = StringField(required=True)

mongo_db = os.getenv("BLOG_DATABASE_NAME")
logging.critical(mongo_db)
mongo_host = os.getenv("DB_HOST")
logging.critical(mongo_host)
mongo_user = os.getenv("DB_USERNAME")
logging.critical(mongo_user)
sleep_time = os.getenv("SLEEP_TIME", default=0)
logging.critical(sleep_time)

connect(db=mongo_db,
        host=mongo_host,
        username=mongo_user,
        password="")

#Return a single activity by a specified ID.
@app.route('/api/activities/<int:id>', methods=["GET"])
def activity(id):
    logging.critical(ActivityLogger.objects.get(id=id).to_json())
    return jsonify(ActivityLogger.objects.get(id=id).to_json()), 201


#Returns all activities in a list.
@app.route('/api/activities/', methods=["GET"])
def activities():
    logs = ActivityLogger.objects.all().order_by("-timestamp").limit(10)
    return jsonify({"activitylog":logs.to_json()}), 201

#Create a new activity, should not contain an ID that is system generated.
#Return the created activity as JSON with ID populated.
@app.route("/api/activities/", methods=["POST"])
def new_activity():
    if not request.json:
        abort(400, "Request not in JSON format")
    new_activity = request.get_json()
    if "username" not in new_activity or "user_id" not in new_activity or "details" not in new_activity:
        abort(400, "Missing key elements")

    # Base instance of ativity log
    activity_log = ActivityLogger(
        user_id=new_activity["user_id"],
        username=new_activity["username"],
        details=new_activity["details"],
    )
    logging.critical(url_for("new_activity", id=id))

    # Saves base instance
    activity_log.save()
    time.sleep(int(sleep_time))
    logging.critical(new_activity)
    return jsonify(new_activity), 201
