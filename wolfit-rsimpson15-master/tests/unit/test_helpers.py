#!/usr/bin/env python3
import pytest
from datetime import datetime, timedelta
from app.helpers import pretty_date


def test_justaboutnow():
    assert pretty_date(datetime.utcnow() - timedelta(seconds=5)) == "just now"

def test_secondsago():
    assert pretty_date(datetime.utcnow() - timedelta(seconds=30)) == "30 seconds ago"

def test_anhourago():
    assert pretty_date(datetime.utcnow() - timedelta(hours=1)) == "an hour ago"

def test_aminuteago():
    assert pretty_date(datetime.utcnow() - timedelta(minutes=1)) == "a minute ago"

def test_minutesago():
    assert pretty_date(datetime.utcnow() - timedelta(minutes=3)) == "3 minutes ago"

def test_anhourago():
    assert pretty_date(datetime.utcnow() - timedelta(hours=1)) == "an hour ago"

def test_hoursago():
    assert pretty_date(datetime.utcnow() - timedelta(hours=3)) == "3 hours ago"

def test_future():
    assert pretty_date(datetime.utcnow() + timedelta(days=1)) == "in the future"

def test_yesterday():
    assert pretty_date(datetime.utcnow() - timedelta(days=1)) == "Yesterday"

def test_daysago():
    assert pretty_date(datetime.utcnow() - timedelta(days=3)) == "3 days ago"

def test_weeksago():
    assert pretty_date(datetime.utcnow() - timedelta(weeks=3)) == "3 weeks ago"

def test_monthsago():
    assert pretty_date(datetime.utcnow() - timedelta(weeks=13)) == "3 months ago"

def test_yearsago():
    assert pretty_date(datetime.utcnow() - timedelta(days=366)) == "1 years ago"