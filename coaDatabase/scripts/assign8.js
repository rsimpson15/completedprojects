/*File Name: assign8.js 
Date: 05/21/2014 
Programmer: Rochelle Simpson*/

$(document).ready(function(){
	$('#char-roster').dataTable({
		'sPaginationType': 'full_numbers',
		'bJQueryUI': true,
		'bFilter': false,
		'aaSorting': [[4, 'desc']]
	});
});
