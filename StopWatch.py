#Assignment 5.2
#Create a stopwatch program. Impliment start, stop, lap, reset and quit
#The program should print elapsed times whenever the stop or lap function are
#used. When the user chooses to quite the program it should write a log
#file containing all the time data (even and time) acquired during the session
#in human readable format.

import time

start_min = 00
start_sec = 00

code = ""
lap_count = 0
lap_sec = 0
lap_min = 0

print("Stop Watch Program")
print()
print("S will start the watch counter.")
print("X will stop the watch.")
print("L will input a lap count and the time to complete it.")
print("R will reset the timer.")
print("Q will exit the program.")
print()

while code != "Q":
    code = input(">").upper()
    if code == "S":
        start_time = round(time.time())
        print(start_min, ":", start_sec)
    elif code == "L":
        lap_marker = round(time.time())
        lap_sec = lap_marker - start_time
        lap_count +=1
        print ("Lap ", lap_count, " - ", "%02d:%02d" % (lap_min, lap_sec))
    elif code == "R":
        print (start_min, ":", start_sec)
    elif code == "X":
        stop_marker = round(time.time())
        stop_time = stop_marker - start_time
        print (stop_time)
    elif code == "Q":
        print("Have a nice day.")
        break;
        
    
