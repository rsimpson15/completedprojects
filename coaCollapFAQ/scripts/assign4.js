/* File Name: assign4.js
Date: 04/23/2014
Programmer: Rochelle Simpson */

$(document).ready(function(){
	dynamicFaq();
});

function dynamicFaq(){
	$('dd').hide();
	$('dt').bind('click', function(){
		$(this).toggleClass('open').next().slideToggle();
	});
}